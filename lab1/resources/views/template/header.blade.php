<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Лаба 1</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <style>
        .container__logo{
            width: 75px;
            height: 75px;;
        }
    </style>
</head>
<body class="d-flex flex-column min-vh-100">
    <div class="container">
        <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
            <img src="/img/logo.svg" alt="logo" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto container__logo">
            <ul class="nav nav-pills align-items-center">
                <li class="nav-item">
                    <a href="/" class="nav-link">Главная</a>
                </li>
                <li class="nav-item">
                    <a href="/about" class="nav-link">О нас</a>
                </li>
                <li class="nav-item">
                    <a href="/news" class="nav-link">Новости</a>
                </li>
            </ul>
        </header>
    </div>
    
</body>


</html>